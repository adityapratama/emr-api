const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const { Op } = require('sequelize');
const passport = require('../../config/passport');

const { serverErrorMessage } = require('../../helpers/response.helper');
const { User, EmailVerificationToken } = require('../../database/models');

const saltRounds = 10;
const charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

const transporter = require('../../config/nodemailer');

const createToken = () => {
  let token = '';
  for (let i = 0; i < 6; i += 1) {
    token += charSet[Math.floor(Math.random() * charSet.length)];
  }
  return token;
};

const createMailOptions = (email, token) => ({
  from: '"EMR" <aditya.psantoso6@gmail.com>',
  to: email,
  subject: 'Email Verification',
  generateTextFromHTML: true,
  html: `Your verification code is <b>${token}</b>`,
});

const sendEmailVerification = (userId, email) => {
  const token = createToken();
  const mailOptions = createMailOptions(email, token);
  if (token.length === 6) {
    EmailVerificationToken.create({
      userId,
      token,
    })
      .then(() => {
        transporter.sendMail(mailOptions, () => {
          transporter.close();
        });
      });
  }
};

const resendEmailVerification = (req, res) => {
  const token = createToken();
  let mailOptions;
  if (req.body.id === undefined || req.body.email === undefined) {
    res.status(400).json({ message: 'Bad request' });
  } else if (token.length === 6) {
    mailOptions = createMailOptions(req.body.email, token);
    EmailVerificationToken.findOne({ where: { userId: req.body.id } })
      .then((verification) => {
        if (!verification) {
          EmailVerificationToken.create({
            userId: req.body.id,
            token,
          })
            .then(() => {
              transporter.sendMail(mailOptions, (errSendEmail) => {
                if (errSendEmail) {
                  serverErrorMessage(res, errSendEmail);
                } else {
                  res.status(200).json({ message: 'We have sent you a new verification code' });
                }
              });
            })
            .catch((errCreate) => {
              serverErrorMessage(res, errCreate);
            });
        } else {
          verification.update({
            token,
            attempt: verification.attempt + 1,
          })
            .then(() => {
              transporter.sendMail(mailOptions, (errSendEmail) => {
                if (errSendEmail) {
                  serverErrorMessage(res, errSendEmail);
                } else {
                  res.status(200).json({ message: 'We have sent you a new verification code' });
                }
              });
            })
            .catch((errUpdate) => {
              serverErrorMessage(res, errUpdate);
            });
        }
      })
      .catch((errFind) => {
        serverErrorMessage(res, errFind);
      });
  } else {
    serverErrorMessage(res, 'The length of token does not amount to 6');
  }
};

const verifyUser = (req, res) => {
  if (req.body.id === undefined || req.body.token === undefined) {
    res.status(400).json({ message: 'Bad request' });
  } else {
    EmailVerificationToken.findOne({ where: { userId: req.body.id } })
      .then((verification) => {
        if (!verification) {
          res.status(404).json({ message: 'Verification not found' });
        } else if (verification.attempt >= 100) {
          res.status(403).json({
            message:
            'You have tried too much to verify the code. For security reason, you need to contact our support for further action',
          });
        } else if (verification.token === req.body.token) {
          const currentTime = moment().valueOf();
          const tokenTime = moment(verification.updatedAt).valueOf();
          const timeDifference = (currentTime - tokenTime) / 1000;
          if (timeDifference > 3600) {
            res.status(403).json({ message: 'This code has expired. Please kindly generate a new code below' });
          } else {
            User.findOne({ where: { id: verification.userId } })
              .then((user) => user.update({ isVerified: true }))
              .then(() => verification.destroy())
              .then(() => res.status(200).json({ message: 'Verification success!' }))
              .catch((err) => {
                serverErrorMessage(res, err);
              });
          }
        } else {
          res.status(401).json({ message: 'Wrong verification code' });
          verification.update({ attempt: verification.attempt + 1 })
            .catch((errUpdate) => {
              serverErrorMessage(res, errUpdate);
            });
        }
      })
      .catch((errFind) => {
        serverErrorMessage(res, errFind);
      });
  }
};

const createUser = (req, res) => {
  if (req.body.id === undefined || req.body.email === undefined
    || req.body.password === undefined || req.body.name === undefined
    || req.body.phone === undefined) {
    res.status(400).json({ message: 'Bad request' });
  } else {
    User.findOne({ where: { [Op.or]: [{ id: req.body.id }, { email: req.body.email }] } })
      .then((user) => {
        if (user !== null) {
          if (user.id === req.body.id) {
            res.status(409).json({ message: 'ID already exist' });
          } else {
            res.status(409).json({ message: 'Email already exist' });
          }
        } else {
          bcrypt.hash(req.body.password, saltRounds, (errHash, hash) => {
            if (errHash) {
              res.status(500).json({ message: errHash });
            } else {
              req.body.password = hash;
              User.create({
                id: req.body.id,
                email: req.body.email,
                password: req.body.password,
                name: req.body.name,
                phone: req.body.phone,
              })
                .then((createdUser) => {
                  if (process.env.NODE_ENV === 'production') {
                    res.status(201).json({ message: 'User created!' });
                    sendEmailVerification(createdUser.id, createdUser.email);
                  } else {
                    res.status(201).json({ message: 'User created!' });
                  }
                })
                .catch((errCreate) => {
                  serverErrorMessage(res, errCreate);
                });
            }
          });
        }
      })
      .catch((errFind) => {
        serverErrorMessage(res, errFind);
      });
  }
};

const loginUser = (req, res) => {
  if (req.body.id === undefined || req.body.password === undefined) {
    res.status(400).json({ message: 'Bad request' });
  } else {
    passport.authenticate('local', (errAuth, user) => {
      if (errAuth) {
        serverErrorMessage(res, errAuth);
      } else if (!user) {
        res.status(401).json({ message: 'Incorrect credential' });
      } else if (user.isVerified === false) {
        res.status(403).json({
          message: 'Your account have not been verified. We have sent the verification code and please fill the space below with the code',
        });
      } else {
        req.login(user, { session: false }, (errLogin) => {
          if (errLogin) {
            serverErrorMessage(res, errLogin);
          } else {
            const body = { id: user.dataValues.id, email: user.dataValues.email };
            const token = jwt.sign({ user: body }, process.env.SECRET_KEY, { expiresIn: 600 });
            res.status(200).json({ token });
          }
        });
      }
    })(req, res);
  }
};

module.exports = {
  sendEmailVerification,
  resendEmailVerification,
  createUser,
  loginUser,
  verifyUser,
};
