const path = require('path');
const fs = require('fs-extra');

const { Op, fn, col } = require('sequelize');

const { IncidentHeader, IncidentDetail } = require('../../database/models');
const { serverErrorMessage } = require('../../helpers/response.helper');

const getImage = (req, res) => {
  IncidentHeader.findOne({ where: { id: req.params.id } })
    .then((header) => {
      if (!header) {
        res.status(404).json({ message: 'Incident Header not found' });
      } else {
        fs.readFile(header.image, (errReadFile, data) => {
          if (errReadFile) {
            serverErrorMessage(res, errReadFile);
          } else {
            res.contentType('image/png');
            res.status(200).send(data);
          }
        });
      }
    })
    .catch((errFind) => {
      serverErrorMessage(res, errFind);
    });
};

const verifyIncidentHeader = (req, res) => {
  IncidentHeader.findOne({ where: { id: req.params.id } })
    .then((header) => {
      if (!header) {
        res.status(404).json({ message: 'Incident Header not found' });
      } else {
        header.update({ statusResponse: 10 })
          .then(() => {
            res.status(204).send();
          })
          .catch((errUpdate) => {
            serverErrorMessage(res, errUpdate);
          });
      }
    })
    .catch((errFind) => {
      serverErrorMessage(res, errFind);
    });
};

const closeIncidentHeader = (req, res) => {
  IncidentHeader.findOne({ where: { id: req.params.id } })
    .then((header) => {
      if (!header) {
        res.status(404).json({ message: 'Incident Header not found' });
      } else {
        header.update({ statusResponse: 90 })
          .then(() => {
            res.status(204).send();
          })
          .catch((errUpdate) => {
            serverErrorMessage(res, errUpdate);
          });
      }
    })
    .catch((errFind) => {
      serverErrorMessage(res, errFind);
    });
};

const getAllIncidentHeaders = (req, res) => {
  IncidentHeader.findAll(
    {
      where: { statusResponse: { [Op.eq]: 10 } },
      attributes: ['id', 'location', 'explanation', 'level', 'createdAt', [fn('COUNT', col('detail.id')), 'detailCount']],
      include: [
        {
          model: IncidentDetail,
          as: 'detail',
          attributes: [],
        },
      ],
      group: ['IncidentHeader.id'],
    },
  )
    .then((headers) => {
      res.status(200).json(headers);
    })
    .catch((err) => {
      serverErrorMessage(res, err);
    });
};

const createIncidentHeader = (req, res) => {
  if (req.file === undefined) {
    res.status(400).json({ message: 'Bad Request' });
  } else if (req.body.userId === undefined || req.body.location === undefined
    || req.body.explanation === undefined || req.body.level === undefined) {
    res.status(400).json({ message: 'Bad Request' });
    fs.unlink(req.file.path);
  } else if (path.extname(req.file.originalname).toLowerCase() === '.png') {
    IncidentHeader.create({
      userId: req.body.userId,
      location: req.body.location,
      image: req.file.path,
      explanation: req.body.explanation,
      level: req.body.level,
    })
      .then(() => {
        res.status(201).json({ message: 'Incident Header created!' });
      })
      .catch((errCreate) => {
        serverErrorMessage(res, errCreate);
        fs.unlink(req.file.path);
      });
  } else {
    fs.unlink(req.file.path, (errDeleteFile) => {
      if (errDeleteFile) {
        serverErrorMessage(res, errDeleteFile);
      } else {
        res.status(403).json({ message: 'Only .png files are allowed' });
      }
    });
  }
};

const createIncidentDetail = (req, res) => {
  if (req.body.userId === undefined || req.body.headerId === undefined
    || req.body.sequence === undefined || req.body.response === undefined) {
    res.status(400).json({ message: 'Bad Request' });
  } else {
    IncidentDetail.create({
      userId: req.body.userId,
      headerId: req.body.headerId,
      sequence: req.body.sequence,
      response: req.body.response,
    })
      .then(() => {
        res.status(201).json({ message: 'Your response has been saved' });
      })
      .catch((err) => {
        serverErrorMessage(res, err);
      });
  }
};

module.exports = {
  getImage,
  verifyIncidentHeader,
  closeIncidentHeader,
  getAllIncidentHeaders,
  createIncidentHeader,
  createIncidentDetail,
};
