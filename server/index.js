const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const router = require('../router');

require('pg').defaults.parseInt8 = true;

const server = express();

server.use(express.json());

server.use(bodyParser.urlencoded({ extended: true }));

if (process.env.NODE_ENV === 'development') {
  server.use(morgan('dev'));
}

server.use(router);

module.exports = server;
