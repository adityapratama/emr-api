const { expect } = require('chai');
const ResMock = require('./mocks/response.mock');

const { serverErrorMessage } = require('../../helpers/response.helper');

const res = new ResMock();

afterEach(() => {
  res.reset();
});

describe('Test Server Error Message', () => {
  it('should return response 500 with message', async () => {
    serverErrorMessage(res, 'Dummy error message');
    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
});
