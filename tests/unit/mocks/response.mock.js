class ResMock {
  constructor() {
    this.body = null;
    this.statusCode = null;
  }

  status(code) {
    this.statusCode = code;
    return this;
  }

  json(message) {
    this.body = message;
    return this;
  }

  send(message) {
    this.body = message;
    return this;
  }

  reset() {
    this.body = null;
    this.statusCode = null;
  }
}

module.exports = ResMock;
