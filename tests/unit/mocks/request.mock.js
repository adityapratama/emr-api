class ReqMock {
  constructor() {
    this.body = {};
    this.params = {};
  }

  setBody(body) {
    this.body = body;
  }

  setParams(params) {
    this.params = params;
  }

  reset() {
    this.body = {};
    this.params = {};
  }
}

module.exports = ReqMock;
