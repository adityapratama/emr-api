require('dotenv').config();

const fs = require('fs-extra');
const path = require('path');
const bcrypt = require('bcrypt');
const request = require('supertest');
const { expect } = require('chai');

const app = require('../../server');
const models = require('../../database/models');

const agent = request.agent(app);

let jwtTokenUser;
let jwtTokenAdmin;

before(() => {
  bcrypt.hash('adminpassword', 10, (err, hash) => {
    models.User.create({
      id: 'adminid',
      email: 'admin@example.com',
      password: hash,
      name: 'adminname',
      phone: 1234,
      isVerified: true,
      isAdmin: true,
    });
  });
});

after(() => {
  models.sequelize.close();
});

describe('Create User', () => {
  it('should create a new user', async () => {
    const res = await agent
      .post('/register')
      .send({
        id: 'userid',
        email: 'user@example.com',
        password: 'userpassword',
        name: 'username',
        phone: 1234,
      });
    expect(res.statusCode).to.equal(201);
    expect(res.body).to.have.property('message');
  });
  it('should not create a new user (same id)', async () => {
    const res = await agent
      .post('/register')
      .send({
        id: 'userid',
        email: 'user2@Example.com',
        password: 'user2password',
        name: 'user2name',
        phone: 1234,
      });
    expect(res.statusCode).to.equal(409);
    expect(res.body).to.have.property('message');
  });
  it('should not create a new user (same email)', async () => {
    const res = await agent
      .post('/register')
      .send({
        id: 'user2id',
        email: 'user@example.com',
        password: 'user2password',
        name: 'user2name',
        phone: 1234,
      });
    expect(res.statusCode).to.equal(409);
    expect(res.body).to.have.property('message');
  });
  it('should return bad request because invalid request body', async () => {
    const res = await agent
      .post('/register')
      .send({
        email: 'user2@example.com',
        name: 'user2name',
        phone: 1234,
      });
    expect(res.statusCode).to.equal(400);
    expect(res.body).to.have.property('message');
  });
});

describe('Login Unverified User', () => {
  it('should not login a user', async () => {
    const res = await agent
      .post('/login')
      .send({
        id: 'userid',
        password: 'userpassword',
      });
    expect(res.statusCode).to.equal(403);
    expect(res.body).to.have.property('message');
  });
});

describe('Verify User', () => {
  before(() => {
    models.EmailVerificationToken.create({
      userId: 'userid',
      token: 'aT509x',
    });
  });
  it('should return bad request because invalid request body', async () => {
    const res = await agent
      .post('/verify')
      .send({
        token: 'aT509x',
      });
    expect(res.statusCode).to.equal(400);
    expect(res.body).to.have.property('message');
  });
  it('should return 404 email verification not found', async () => {
    const res = await agent
      .post('/verify')
      .send({
        id: 'nonexistuserid',
        token: 'aT509x',
      });
    expect(res.statusCode).to.equal(404);
    expect(res.body).to.have.property('message');
  });
  it('should successfully verify a user', async () => {
    const res = await agent
      .post('/verify')
      .send({
        id: 'userid',
        token: 'aT509x',
      });
    expect(res.statusCode).to.equal(200);
    expect(res.body).to.have.property('message');
  });
});

describe('Login Verified User', () => {
  it('should login a user', async () => {
    const res = await agent
      .post('/login')
      .send({
        id: 'userid',
        password: 'userpassword',
      });
    expect(res.statusCode).to.equal(200);
    expect(res.body).to.have.property('token');

    // SET USER JWT TOKEN TO HEADER
    jwtTokenUser = res.body.token;
    agent.set('Authorization', `Bearer ${jwtTokenUser}`);
  });
});

describe('User request to forbidden routes', () => {
  it('should return 403 forbidden (verify incident header)', async () => {
    const res = await agent.patch('/incident-header/verify/1');
    expect(res.statusCode).to.equal(403);
    expect(res.body).to.have.property('message');
  });
  it('should return 403 forbidden (close incident header)', async () => {
    const res = await agent.patch('/incident-header/close/1');
    expect(res.statusCode).to.equal(403);
    expect(res.body).to.have.property('message');
  });
});

describe('Create Incident Header', () => {
  it('should created new incident header', async () => {
    const res = await agent
      .post('/incident-header')
      .attach('image', path.join(__dirname, '/assets/images/test.png'))
      .field('userId', 'userid')
      .field('location', 'dummyLocation')
      .field('explanation', 'dummyExplanation')
      .field('level', 10);
    expect(res.statusCode).to.equal(201);
    expect(res.body).to.have.property('message');
  });
  after(() => {
    const uploadDirPath = path.join(__dirname, '../../uploads/test');
    fs.readdir(uploadDirPath, (errUpload, files) => {
      for (let i = 0; i < files.length; i += 1) {
        fs.unlink(path.join(uploadDirPath, files[i]));
      }
    });
  });
});

describe('Get Unverified Incident Header', () => {
  it('should created new incident header', async () => {
    const res = await agent.get('/incident-header');
    expect(res.statusCode).to.equal(200);
    expect(res.body.length).to.equal(0);
  });
});

// CHANGE USER AS ADMIN
describe('Login Admin', () => {
  it('should login a admin', async () => {
    const res = await agent
      .post('/login')
      .send({
        id: 'adminid',
        password: 'adminpassword',
      });
    expect(res.statusCode).to.equal(200);
    expect(res.body).to.have.property('token');

    // SET ADMIN JWT TOKEN TO HEADER
    jwtTokenAdmin = res.body.token;
    agent.set('Authorization', `Bearer ${jwtTokenAdmin}`);
  });
});

describe('Verify Incident Header', () => {
  it('should return 404 incident header not found', async () => {
    const res = await agent.patch('/incident-header/verify/2');
    expect(res.statusCode).to.equal(404);
    expect(res.body).to.have.property('message');
  });
  it('should verify a incident header', async () => {
    const res = await agent.patch('/incident-header/verify/1');
    expect(res.statusCode).to.equal(204);
  });
});

describe('Get Verified Incident Header', () => {
  before(() => {
    agent.set('Authorization', `Bearer ${jwtTokenUser}`);
  });
  it('should created new incident header', async () => {
    const res = await agent.get('/incident-header');
    expect(res.statusCode).to.equal(200);
    expect(res.body.length).to.equal(1);
  });
});

describe('Create Incident Detail', () => {
  it('should create new incident detail', async () => {
    let res;
    res = await agent
      .post('/incident-detail')
      .send({
        userId: 'userid',
        headerId: 1,
        sequence: 1,
        response: 'Dummy response',
      });
    expect(res.statusCode).to.equal(201);
    expect(res.body).to.have.property('message');

    res = await agent.get('/incident-header');
    expect(res.statusCode).to.equal(200);
    expect(res.body[0]).to.have.property('detailCount');
    expect(res.body[0].detailCount).to.equal(1);
  });
});

describe('Close Incident Header', () => {
  before(() => {
    agent.set('Authorization', `Bearer ${jwtTokenAdmin}`);
  });
  it('should close a incident header', async () => {
    const res = await agent.patch('/incident-header/close/1');
    expect(res.statusCode).to.equal(204);
  });
});
