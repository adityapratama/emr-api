const { Router } = require('express');

const router = Router();
const userRouter = require('./user/user.route');
const incidentRouter = require('./incident/incident.route');

router.use(userRouter);
router.use(incidentRouter);

module.exports = router;
