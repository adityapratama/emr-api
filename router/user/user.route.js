const { Router } = require('express');

const controller = require('../../controllers/user/user.controller');

const router = Router();
router.post('/register', controller.createUser);
router.post('/login', controller.loginUser);
router.post('/verify', controller.verifyUser);
router.post('/resend-verification', controller.resendEmailVerification);

module.exports = router;
