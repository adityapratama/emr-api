const multer = require('multer');

const passport = require('../../config/passport');
const { serverErrorMessage } = require('../../helpers/response.helper');

let destUpload;
if (process.env.NODE_ENV === 'test') {
  destUpload = './uploads/test';
} else {
  destUpload = './uploads';
}

const upload = multer({ dest: destUpload }).single('image');

const isAuthorized = (req, res, next) => {
  passport.authenticate('jwt', { session: false }, (err, user) => {
    if (err) {
      serverErrorMessage(res, err);
    } else if (!user) {
      res.status(403).json({ message: 'Not yet login' });
    } else {
      req.admin = user.isAdmin;
      next();
    }
  })(req, res);
};

const isAdmin = (req, res, next) => {
  if (req.admin) {
    next();
  } else {
    res.status(403).json({ message: 'Forbidden' });
  }
};

const multerMiddleware = (req, res, next) => {
  upload(req, res, (err) => {
    if (err) {
      serverErrorMessage(res, err);
    } else {
      next();
    }
  });
};

module.exports = {
  isAuthorized,
  isAdmin,
  multerMiddleware,
};
