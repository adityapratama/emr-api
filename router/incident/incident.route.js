const { Router } = require('express');

const { multerMiddleware, isAuthorized, isAdmin } = require('../middleware');
const controller = require('../../controllers/incident/incident.controller');

const router = Router();
router.get('/incident-header/image/:id(\\d+)', controller.getImage);
router.patch('/incident-header/verify/:id(\\d+)', isAuthorized, isAdmin, controller.verifyIncidentHeader);
router.patch('/incident-header/close/:id(\\d+)', isAuthorized, isAdmin, controller.closeIncidentHeader);
router.get('/incident-header', isAuthorized, controller.getAllIncidentHeaders);
router.post('/incident-header', isAuthorized, multerMiddleware, controller.createIncidentHeader);
router.post('/incident-detail', isAuthorized, controller.createIncidentDetail);

module.exports = router;
