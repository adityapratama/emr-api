
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    name: DataTypes.STRING,
    phone: DataTypes.INTEGER,
    level: DataTypes.INTEGER,
    isVerified: DataTypes.BOOLEAN,
    isAdmin: DataTypes.BOOLEAN,
  }, {});
  User.associate = (models) => {
    // associations can be defined here
    User.hasMany(models.IncidentHeader, {
      foreignKey: 'userId',
      as: 'header',
      onDelete: 'CASCADE',
    });
    User.hasMany(models.IncidentDetail, {
      foreignKey: 'userId',
      as: 'detail',
      onDelete: 'CASCADE',
    });
  };
  return User;
};
