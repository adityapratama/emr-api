
module.exports = (sequelize, DataTypes) => {
  const IncidentDetail = sequelize.define('IncidentDetail', {
    userId: DataTypes.STRING,
    headerId: DataTypes.INTEGER,
    sequence: DataTypes.INTEGER,
    response: DataTypes.STRING,
  }, {});
  IncidentDetail.associate = (models) => {
    // associations can be defined here
    IncidentDetail.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user',
    });
    IncidentDetail.belongsTo(models.IncidentHeader, {
      foreignKey: 'headerId',
      as: 'header',
    });
  };
  return IncidentDetail;
};
