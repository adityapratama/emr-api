
module.exports = (sequelize, DataTypes) => {
  const EmailVerificationToken = sequelize.define('EmailVerificationToken', {
    userId: DataTypes.STRING,
    token: DataTypes.STRING,
    attempt: DataTypes.INTEGER,
  }, {});
  EmailVerificationToken.associate = (models) => {
    // associations can be defined here
    EmailVerificationToken.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user',
    });
  };
  return EmailVerificationToken;
};
