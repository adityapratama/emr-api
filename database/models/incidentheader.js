
module.exports = (sequelize, DataTypes) => {
  const IncidentHeader = sequelize.define('IncidentHeader', {
    userId: DataTypes.STRING,
    location: DataTypes.STRING,
    image: DataTypes.STRING,
    explanation: DataTypes.STRING,
    level: DataTypes.INTEGER,
    statusClose: DataTypes.BOOLEAN,
    statusResponse: DataTypes.INTEGER,
  }, {});
  IncidentHeader.associate = (models) => {
    // associations can be defined here
    IncidentHeader.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user',
    });
    IncidentHeader.hasMany(models.IncidentDetail, {
      foreignKey: 'headerId',
      as: 'detail',
      onDelete: 'CASCADE',
    });
  };
  return IncidentHeader;
};
