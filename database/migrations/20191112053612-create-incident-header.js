
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('IncidentHeaders', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    userId: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    location: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    image: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    explanation: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    level: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    statusClose: {
      defaultValue: false,
      type: Sequelize.BOOLEAN,
    },
    statusResponse: {
      defaultValue: 0,
      type: Sequelize.INTEGER,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('IncidentHeaders'),
};
