
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('IncidentDetails', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    userId: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    headerId: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    sequence: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    response: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('IncidentDetails'),
};
