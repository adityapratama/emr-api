
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('EmailVerificationTokens', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    userId: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    token: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    attempt: {
      defaultValue: 0,
      type: Sequelize.INTEGER,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('EmailVerificationTokens'),
};
