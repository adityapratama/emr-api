const bcrypt = require('bcrypt');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');

const { User } = require('../database/models');

const passportLocalOption = {
  usernameField: 'id',
  passwordField: 'password',
};

const passportJwtOption = {
  secretOrKey: process.env.SECRET_KEY,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};

passport.use(new LocalStrategy(passportLocalOption, (id, password, done) => {
  User.findOne({ where: { id } })
    .then((user) => {
      if (!user) {
        done(null, false);
      } else {
        bcrypt.compare(password, user.password, (_, res) => {
          if (!res) {
            done(null, false);
          } else {
            done(null, user);
          }
        });
      }
    })
    .catch((err) => done(err));
}));

passport.use(new JwtStrategy(passportJwtOption, (jwtPayload, done) => {
  User.findOne({ where: { id: jwtPayload.user.id } })
    .then((user) => {
      if (!user) {
        done(null, false);
      } else {
        done(null, user);
      }
      return null;
    })
    .catch((err) => done(err, false));
}));

module.exports = passport;
