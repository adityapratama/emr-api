const serverErrorMessage = (res, err) => {
  if (process.env.NODE_ENV === 'development') {
    console.log(err);
  }
  res.status(500).json({ message: 'Something wrong, please try again later or contact our support' });
};

module.exports = {
  serverErrorMessage,
};
